package com.example.ktomaszewski.tempartuino;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private WebView webView1;
    private Button alarmOn;
    private Button alarmOff;

    private static final String host = "https://www.google.pl/search?q=";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeViewElement();

        webView1.setWebViewClient(new WebViewClient());
        webView1.loadUrl(host);

        alarmOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView1.loadUrl(host + "alarmOn");
            }
        });

        alarmOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView1.loadUrl(host + "alarmOf");
            }
        });
    }

    private void initializeViewElement() {
        webView1 = (WebView) findViewById(R.id.webView1);
        alarmOn = (Button) findViewById(R.id.alarmOn);
        alarmOff = (Button) findViewById(R.id.alarmOff);
    }
}
